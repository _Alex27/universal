const profundidadPozo = 45;
let pasos = 0;
let dia = 0;

while (pasos <= profundidadPozo) {
  pasos += 7 - 2;
  dia++;
  console.log("En el dia " + dia + " el caracol recorrió " + pasos);
  if (pasos >= profundidadPozo) {
    break;
  }
}
