// let hora = 0;
// let cu = "CUCÚ! ";

// for (let hora = 0; hora < 24; hora++) {
//     let sonlas = ("Son las " + hora + ":00");
//     if (hora < 8 || hora > 22) {
//         console.log(sonlas);
//     } else if (hora >= 8 && hora <= 22) {
//         let cucu = ("Son las " + hora + ":00. " + cu);
//         console.log(cucu);
//         if (hora >= 8 && hora <= 12) {
//             cu += "CUCÚ! ";
//         } else if (hora > 12 && hora < 22) {
//             cu = "CUCÚ! ";
//             cu += "CUCÚ! ";
//         }
//     } else {
//         console.log("Error");
//     }
// }

let hora = 0;
let cu = "CUCÚ! ";

for (let hora = 0; hora < 24; hora++) {
  let sonlas = "Son las " + hora + ":00";
  if (hora < 8 || hora > 22) {
    console.log(sonlas);
  } else if (hora >= 8 && hora <= 22) {
    let cucuString = "";
    let patata;
    if (hora > 12) {
      patata = hora - 12;
    } else {
      patata = hora;
    }
    for (; patata > 0; patata--) {
      cucuString += cu;
    }
    console.log(sonlas + ". " + cucuString);
  } else {
    console.log("Error");
  }
}

for (let hora = 0; hora < 24; hora++) {
  let sonlas = "Son las " + hora + ":00";
  console.log(sonlas);
  if (hora >= 8 && hora <= 22) {
    let cucuString = "CUCÚ! ".repeat(hora > 12 ? hora - 12 : hora);
    console.log(cucuString);
  }
}
