// Esta es una línea de comentario
/* Esto es un bloque de comentario */

// Esta es la función que muestra el mensaje
function mostrarMensaje() {
    // Esta es la variable que guarda el mensaje
    var mensaje = "¡Hola! Bienvenido a esta página web.";
    // Esta es la función que muestra una ventana emergente con el mensaje
    alert(mensaje);
  }
  
  // Esta es la línea que llama a la función cuando se carga la página
  window.onload = mostrarMensaje;




  // Esta es la variable que guarda el radio del círculo
var radio = 5;
// Esta es la variable que guarda el área del círculo
var area = Math.PI * Math.pow(radio, 2);
// Esta es la línea que muestra el resultado por consola
console.log("El área del círculo es " + area + " cm2");




// Esta es la variable que guarda el número aleatorio
var numero = Math.floor(Math.random() * 10 + 1);
// Esta es la línea que muestra el resultado por consola
console.log("El número aleatorio es " + numero);



// Esta es la variable que guarda la edad
var edad = 20;
// Esta es la variable que guarda el resultado de la comparación
var mayorDeEdad = edad >= 18;
// Esta es la línea que muestra el resultado por consola
console.log("¿Es mayor de edad? " + mayorDeEdad);

// El resultado sería:

// ¿Es mayor de edad? true