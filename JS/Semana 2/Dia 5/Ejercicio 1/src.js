let coche = {
  modelo: "60",
  marca: "Opel",
  color: "Rojo",
};

console.log(coche);

coche.color = "Azul";
coche.anho = "2007";

console.log(coche.color + coche.anho);

const Respuesta = confirm("¿Quieres ver los valores? (confirm : Sí)");

if (Respuesta === "true") {
  for (const key in coche) {
    const value = coche[key];

    console.log(value);
  }
}
