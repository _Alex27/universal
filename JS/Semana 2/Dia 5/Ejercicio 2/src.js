let colores = ["rojo", "azul", "verde", "amarillo", "naranja"];

console.log(colores);

colores[1] = "morado";
colores[3] = "violeta";

console.log(colores[1] + " " + colores[3]);

colores[colores.length] = "cyan";

console.log(colores);

for (let index = 0; index < colores.length; index++) {
  console.log(colores[index]);
}
