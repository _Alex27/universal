/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dado el array de coches de abajo, muestra por consola:
 *
 *   1. Todos los coches de la marca Audi.
 *
 *   2. Los coches de transmisión manual y de color negro.
 *
 *   3. La media de precio de los coches de marca Ford.
 *
 *   4. La suma total de los precios de todos los coches.
 *
 */

const coches = [
  {
    marca: "BMW",
    modelo: "Serie 3",
    year: 2012,
    precio: 30000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Audi",
    modelo: "A4",
    year: 2018,
    precio: 40000,
    puertas: 4,
    color: "Negro",
    transmision: "automatico",
  },
  {
    marca: "Ford",
    modelo: "Mustang",
    year: 2015,
    precio: 20000,
    puertas: 2,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Audi",
    modelo: "A6",
    year: 2010,
    precio: 35000,
    puertas: 4,
    color: "Negro",
    transmision: "automatico",
  },
  {
    marca: "BMW",
    modelo: "Serie 5",
    year: 2016,
    precio: 70000,
    puertas: 4,
    color: "Rojo",
    transmision: "automatico",
  },
  {
    marca: "Mercedes Benz",
    modelo: "Clase C",
    year: 2015,
    precio: 25000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Chevrolet",
    modelo: "Camaro",
    year: 2018,
    precio: 60000,
    puertas: 2,
    color: "Rojo",
    transmision: "manual",
  },
  {
    marca: "Ford",
    modelo: "Mustang",
    year: 2019,
    precio: 80000,
    puertas: 2,
    color: "Rojo",
    transmision: "manual",
  },
  {
    marca: "Dodge",
    modelo: "Challenger",
    year: 2017,
    precio: 40000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Audi",
    modelo: "A3",
    year: 2017,
    precio: 55000,
    puertas: 2,
    color: "Negro",
    transmision: "manual",
  },
  {
    marca: "Dodge",
    modelo: "Challenger",
    year: 2012,
    precio: 25000,
    puertas: 2,
    color: "Rojo",
    transmision: "manual",
  },
  {
    marca: "Mercedes Benz",
    modelo: "Clase C",
    year: 2018,
    precio: 45000,
    puertas: 4,
    color: "Azul",
    transmision: "automatico",
  },
  {
    marca: "BMW",
    modelo: "Serie 5",
    year: 2019,
    precio: 90000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Ford",
    modelo: "Mustang",
    year: 2017,
    precio: 60000,
    puertas: 2,
    color: "Negro",
    transmision: "manual",
  },
  {
    marca: "Dodge",
    modelo: "Challenger",
    year: 2015,
    precio: 35000,
    puertas: 2,
    color: "Azul",
    transmision: "automatico",
  },
  {
    marca: "BMW",
    modelo: "Serie 3",
    year: 2018,
    precio: 50000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "BMW",
    modelo: "Serie 5",
    year: 2017,
    precio: 80000,
    puertas: 4,
    color: "Negro",
    transmision: "automatico",
  },
  {
    marca: "Mercedes Benz",
    modelo: "Clase C",
    year: 2018,
    precio: 40000,
    puertas: 4,
    color: "Blanco",
    transmision: "automatico",
  },
  {
    marca: "Audi",
    modelo: "A4",
    year: 2016,
    precio: 30000,
    puertas: 4,
    color: "Azul",
    transmision: "automatico",
  },
];

//   const audiCars = coches.filter((car) => car.marca === "Audi");
// console.log("Coches de la marca Audi:", audiCars);

// const manualBlackCars = coches.filter(
//   (car) => car.transmision === "manual" && car.color === "Negro"
// );
// console.log("Coches de transmisión manual y color negro:", manualBlackCars);

// const fordCars = coches.filter((car) => car.marca === "Ford");
// const fordAvgPrice =
//   fordCars.reduce((acc, car) => acc + car.precio, 0) / fordCars.length;
// console.log("Media del precio de los coches Ford:", fordAvgPrice);

// const totalPrice = coches.reduce((acc, car) => acc + car.precio, 0);
// console.log("Suma total del precio de todos los coches:", totalPrice);

let audiCars = [];
let manualBlackCars = [];
let fordCars = [];
let totalPrice = 0;

for (let j = 0; j < coches.length; j++) {
  let car = coches[j];

  if (car.marca === "Audi") {
    audiCars[audiCars.length] = car;
  }

  if (car.transmision === "manual" && car.color === "Negro") {
    manualBlackCars[manualBlackCars.length] = car;
  }

  if (car.marca === "Ford") {
    fordCars[fordCars.length] = car;
  }

  totalPrice += car.precio;
}

console.log("Coches de la marca Audi:", audiCars);
console.log("Coches de transmisión manual y color negro:", manualBlackCars);

let fordAvgPrice = 0;
if (fordCars.length > 0) {
  fordAvgPrice = totalPrice / fordCars.length;
}
console.log("Media del precio de los coches Ford:", fordAvgPrice);

console.log("Suma total del precio de todos los coches:", totalPrice);
