const fruitsBasket = [
  "naranja",
  "naranja",
  "limón",
  "pera",
  "limón",
  "plátano",
  "naranja",
];

const fruitsCount = {};

for (const fruit of fruitsBasket) {
  if (fruitsCount[fruit]) {
    fruitsCount[fruit]++;
  } else {
    fruitsCount[fruit] = 1;
  }
}

console.log(fruitsCount);
