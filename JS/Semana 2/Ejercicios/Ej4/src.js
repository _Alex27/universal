const pizzas = [
  "margarita",
  "cuatro quesos",
  "prosciutto",
  "carbonara",
  "barbacoa",
  "tropical",
];

// function combine(pizzas) {
//   const combinations = [];
//   combinations.push(pizzas);

//   // Escribe aquí tu código

//   return combinations;
// }

function combine(pizzas) {
  const combinations = [];
  for (let i = 0; i < pizzas.length; i++) {
    for (let j = i + 1; j < pizzas.length; j++) {
      let half1 = pizzas[i].slice(0, pizzas[i].length / 2);
      let half2 = pizzas[j].slice(pizzas[j].length / 2);
      let combination = half1.concat(half2);
      combinations.push(combination);
    }
  }
  return combinations;
}
console.log(combine(pizzas));

// function combine(pizzas) {
//     const combinations = [];
//     for (let p1 = 0; p1 < pizzas.length; p1++) {
//       for (let p2 = p1 + 1; p2 < pizzas.length; p2++) {
//         let combination = pizzas[p1] + " y " + pizzas[p2];
//         combinations.push(combination);
//       }
//     }
//     return combinations;
//   }
//   console.log(combine(pizzas));
