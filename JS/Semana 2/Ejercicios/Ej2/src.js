const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

// Escribe aquí tu código

function noDoble(array) {
  const set = new Set(array);

  const newArray = [...set];

  return newArray;
}

const Sinrepetir = noDoble(names);

console.log(Sinrepetir);
