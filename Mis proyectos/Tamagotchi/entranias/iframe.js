let nombre = prompt("¿Cómo se llama tu mascota?");
console.log(nombre);
document.getElementById("nombre").innerHTML = nombre;

if (confirm("¿Estás listo para comenzar a cuidar de " + nombre + "?")) {
  let iframe = document.createElement("iframe");
  iframe.setAttribute("src", "./entranias/index.html");
  document.body.appendChild(iframe);
} else {
  alert("Hasta la próxima!");
}
