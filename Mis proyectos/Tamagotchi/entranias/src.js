const botonCama = document.getElementById("boton-cama");
const botonTele = document.getElementById("boton-tele");
const botonNevera = document.getElementById("boton-nevera");
const barraHambre = document.querySelector("#hambre .progress-bar");
const barraSueño = document.querySelector("#sueño .progress-bar");
const barraAburrimiento = document.querySelector("#aburrimiento .progress-bar");

let valorHambre = 100;
let valorSueño = 100;
let valorAburrimiento = 100;

botonCama.addEventListener("click", () => {
  valorSueño += 25;
  if (valorSueño > 100) {
    valorSueño = 100;
  }
  sueño.animate(valorSueño / 100);
});

botonTele.addEventListener("click", () => {
  valorAburrimiento += 15;
  if (valorAburrimiento > 100) {
    valorAburrimiento = 100;
  }
  aburrimiento.animate(valorAburrimiento / 100);
});

botonNevera.addEventListener("click", () => {
  valorHambre += 10;
  if (valorHambre > 100) {
    valorHambre = 100;
  }
  hambre.animate(valorHambre / 100);
});

const hambre = new ProgressBar.Circle("#hambre", {
  color: "red",
  strokeWidth: 6,
  trailWidth: 1,
  text: {
    value: valorHambre,
  },
});

const sueño = new ProgressBar.Circle("#sueño", {
  color: "blue",
  strokeWidth: 6,
  trailWidth: 1,
  text: {
    value: valorSueño,
  },
});

const aburrimiento = new ProgressBar.Circle("#aburrimiento", {
  color: "green",
  strokeWidth: 6,
  trailWidth: 1,
  text: {
    value: valorAburrimiento,
  },
});

const intervalo = setInterval(actualizarBarras, 1000);

function actualizarBarras() {
  valorHambre -= 1;
  valorSueño -= 2;
  valorAburrimiento -= 3;
  hambre.animate(valorHambre / 100);
  sueño.animate(valorSueño / 100);
  aburrimiento.animate(valorAburrimiento / 100);
  if (valorHambre <= 0 || valorSueño <= 0 || valorAburrimiento <= 0) {
    mascotaFallecida();
  }
}

function reiniciarJuego() {
  valorHambre = 100;
  valorSueño = 100;
  valorAburrimiento = 100;
  hambre.set(valorHambre / 100);
  sueño.set(valorSueño / 100);
  aburrimiento.set(valorAburrimiento / 100);
  intervalo = setInterval(actualizarBarras, 1000);
}

function checkIfDead() {
  const stats = Object.values(mascota);
  const allStatsZero = stats.every((stat) => stat === 0);
  if (allStatsZero) {
    alert("Tu mascota ha fallecido");
    resetGame();
  }
}

function updateStats() {
  updateBar("hambre", mascota.hambre);
  updateBar("sueno", mascota.sueno);
  updateBar("aburrimiento", mascota.aburrimiento);

  checkIfDead();
}

function mascotaFallecida() {
  clearInterval(intervalo);
  alert("Tu mascota ha muerto :(");
}
